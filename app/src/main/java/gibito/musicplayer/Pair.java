package gibito.musicplayer;

public class Pair<T1, T2> {
    T1 left;
    T2 right;

    Pair(T1 left, T2 right) {
        this.left = left;
        this.right = right;
    }
}
