package gibito.musicplayer;

import android.os.Environment;

import com.google.gson.Gson;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Map;


public class MusicDB {
   private ArrayList<Pair<String, String >> db;

    MusicDB() {
        if (dbExists()) openDB();
        else createDB();
    }

    void updateDB() {

    }

    File openFromDB(String name) {
       return new File(Environment.getExternalStorageDirectory(), "/Music/" + name);
    }

    String listDB() {
        String[] names = new String[db.size()];
        for (int i = 0; i < db.size(); i++) {
            names[i] = db.get(i).left;
        }
        return new Gson().toJson(names);
    }

    private void createDB() {

    }

    private void openDB() {

    }

    private boolean dbExists() {
        return new File(Environment.getDataDirectory() + "/db.json").exists();
    }

    Map<String, String> getDB() {
        return db;
    }
}
